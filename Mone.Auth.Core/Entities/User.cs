﻿namespace Mone.Auth.Core.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool EmailConfirmed { get; set; }
        public string NickName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
