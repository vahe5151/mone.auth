﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mone.Auth.Core.Entities
{
    public interface IBaseEntity
    {
        [Key]
        int Id { get; set; }
        bool IsDeleted { get; set; }
        DateTime CreatedDt { get; set; }
        DateTime UpdatedDt { get; set; }
    }
}
