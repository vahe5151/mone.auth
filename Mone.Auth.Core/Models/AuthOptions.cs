﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Mone.Auth.Core.Models
{
    public class AuthOptions
    {
        public const string ISSUER = "AuthServer";
        public const string AUDIENCE = "AuthClient";
        public const string KEY = "TestKeyFor_M'One_Authentication$#$#";
        public const int LIFETIME = 10;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
