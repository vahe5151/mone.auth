﻿using System.ComponentModel.DataAnnotations;

namespace Mone.Auth.Core.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Not specified Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Not specified Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
