﻿using System.ComponentModel.DataAnnotations;

namespace Mone.Auth.Core.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Not specified Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Not specified Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password entered incorrectly")]
        public string ConfirmPassword { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }

    }
}
