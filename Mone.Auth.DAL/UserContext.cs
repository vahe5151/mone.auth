﻿using Microsoft.AspNet.Identity;
using Microsoft.EntityFrameworkCore;
using Mone.Auth.Core.Entities;

namespace Mone.Auth.DAL
{
    public class UserContext : DbContext
    {
        public DbSet<User> Users { get; set; } 
        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<TEntity> WriterSet<TEntity>() where TEntity : class, IBaseEntity
        {
            return base.Set<TEntity>();
        }
    }
}
