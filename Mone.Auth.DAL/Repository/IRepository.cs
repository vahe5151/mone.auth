﻿using Mone.Auth.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mone.Auth.DAL.Repository
{
    public interface IRepository
    {
        #region Sync
        IQueryable<T> GetAll<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        IQueryable<T> Filter<T>(Expression<Func<T, bool>> query) where T : BaseEntity;
        IQueryable<T> FilterAsNoTracking<T>(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        void Update<T>(T entity) where T : BaseEntity;
        #endregion

        #region Async
        Task<T> FindAsync<T>(int id) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllAsNoTrackingAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<T> CreateAsync<T>(T entity) where T : BaseEntity;
        Task<T> GetByIdAsync<T>(long id, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity;
        Task<int> SaveChanges();
        #endregion
        void Dispose();
    }
}
