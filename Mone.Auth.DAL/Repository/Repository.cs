﻿using Microsoft.EntityFrameworkCore;
using Mone.Auth.Core.Entities;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mone.Auth.DAL.Repository
{
    public class Repository : IRepository
    {
        private readonly UserContext db;
        public Repository(UserContext context)
        {
            db = context;
        }

        #region Sync
        public IQueryable<T> GetAll<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity
        {
            var set = db.Set<T>().AsQueryable();
            if (includeExpression.Any())
                set = includeExpression.Aggregate(set, (current, variable) => current.Include(variable));
            return set;
        }

        public IQueryable<T> Filter<T>(Expression<Func<T, bool>> query) where T : BaseEntity
        {
            return db.Set<T>().Where(x => !x.IsDeleted).Where(query);
        }

        public IQueryable<T> FilterAsNoTracking<T>(Expression<Func<T, bool>> query,
           params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity
        {
            if (query == null)
                throw new Exception("Query is null");
            var set = db.Set<T>().Where(query).AsNoTracking();
            if (includeExpression.Any())
                set = includeExpression.Aggregate(set, (current, variable) => current.Include(variable));
            return set;
        }

        public void Update<T>(T entity) where T : BaseEntity
        {
            var now = DateTime.UtcNow;
            entity.UpdatedDt = now;
            db.Update(entity);
        }
        #endregion

        #region Async
        public async Task<IEnumerable<T>> GetAllAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity
        {
            var set = db.Set<T>().AsQueryable();
            if (includeExpression.Any())
                set = includeExpression.Aggregate(set, (current, variable) => current.Include(variable));
            return await set.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsNoTrackingAsync<T>(params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity
        {
            var set = db.Set<T>().AsNoTracking();
            if (includeExpression.Any())
                set = includeExpression.Aggregate(set, (current, include) => current.Include(include));
            return await set.ToListAsync();
        }

        public async Task<T> FindAsync<T>(int id) where T : BaseEntity
        {
            return await db.FindAsync<T>(id);
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : class, IBaseEntity
        {
            var set = db.WriterSet<T>().Where(x => x.Id == id && !x.IsDeleted);
            return await set.FirstOrDefaultAsync();
        }

        public async Task<T> GetByIdAsync<T>(long id, params Expression<Func<T, object>>[] includeExpression) where T : BaseEntity
        {
            var set = db.Set<T>().Where(x => x.Id == id);
            if (includeExpression.Any())
                set = includeExpression.Aggregate(set, (current, variable) => current.Include(variable));
            return await set.FirstOrDefaultAsync();
        }
       
        public async Task<T> CreateAsync<T>(T entity) where T : BaseEntity
        {
            var now = DateTime.UtcNow;
            entity.CreatedDt = now;
            entity.UpdatedDt = now;
            await db.Set<T>().AddAsync(entity);
            return entity;
        }

        public async Task<int> SaveChanges()
        {
            return await db.SaveChangesAsync();
        }
        #endregion
        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
