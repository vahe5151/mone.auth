﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Mone.Auth.Core.Entities;
using Mone.Auth.Core.Helpers;
using Mone.Auth.Core.Models;
using Mone.Auth.DAL.Repository;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Mone.Auth.Services.Services
{
    public class LoginService : ILoginService
    {
        private readonly IRepository _repository;
        private IConfiguration _config;

        public LoginService(IRepository repository, IConfiguration configuration)
        {
            _repository = repository;
            _config = configuration;
        }

        public string Login(LoginModel loginModel)
        {
            var encrypt = Utilities.Encrypt(loginModel.Password);
            var user = _repository.FilterAsNoTracking<User>(x => x.Email == loginModel.Email && x.Password == encrypt).FirstOrDefault();

            if (user != null)
            {
                var token = Generate(user.Email, user.Password);
                return token;
            }

            return "User Not Found";
        }
        private string Generate(string email, string password)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AuthOptions.KEY));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, password),
                new Claim(ClaimTypes.Email,email)
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Audience"],
              claims,
              expires: DateTime.Now.AddMinutes(500),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
