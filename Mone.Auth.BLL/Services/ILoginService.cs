﻿using Mone.Auth.Core.Models;

namespace Mone.Auth.Services.Services
{
    public interface ILoginService
    {
        string Login(LoginModel loginModel);
    }
}
