﻿using Mone.Auth.Core.Models;
using System.Threading.Tasks;

namespace Mone.Auth.Services.Services
{
    public interface IUserService
    {
        Task<AddUserResponseModel> Register(RegisterModel registerModel);
        Task<UserResponseModel> GetUserByIdAsync(int id);
    }
}
