﻿using Mone.Auth.Core.Entities;
using Mone.Auth.Core.Helpers;
using Mone.Auth.Core.Models;
using Mone.Auth.DAL.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Mone.Auth.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository _repository;

        public UserService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<AddUserResponseModel> Register(RegisterModel registerModel)
        {
            var user = _repository.FilterAsNoTracking<User>(x => x.Email == registerModel.Email).FirstOrDefault();
            if (user == null)
            {
                var adduser = new User
                {
                    Email = registerModel.Email,
                    Password = Utilities.Encrypt(registerModel.Password),
                    PhoneNumber = registerModel.PhoneNumber,
                    UserName = registerModel.UserName,
                };

                await _repository.CreateAsync(adduser);
                await _repository.SaveChanges();

                return new AddUserResponseModel
                {
                    Id = adduser.Id,
                    Email = adduser.Email,
                    PhoneNumber = adduser.PhoneNumber,
                    UserName = adduser.UserName
                };
            }
            else
            {
                throw new Exception("User_Already_Exist");
            }
        }
        public async Task<UserResponseModel> GetUserByIdAsync(int id)
        {
            var user = await _repository.FindAsync<User>(id) ?? throw new Exception("User Not found");
            return new UserResponseModel
            {
                Id = user.Id,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                UserName = user.UserName
            };
        }
    }
}
