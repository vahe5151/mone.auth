﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mone.Auth.Core.Models;
using Mone.Auth.Services.Services;

namespace Mone.Auth.Controllerss
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private ILoginService _loginService;
        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost]
        [AllowAnonymous]
        public string Login(LoginModel model)
        {
            return _loginService.Login(model);
        }
    }
}
