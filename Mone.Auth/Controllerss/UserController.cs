﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Mone.Auth.Core.Models;
using Mone.Auth.DAL;
using Mone.Auth.Services.Services;
using System.Threading.Tasks;

namespace Mone.Auth.Controllerss
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private IUserService _userService;
        public UserController(UserContext context, IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<AddUserResponseModel> Register(RegisterModel model)
        {
            return await _userService.Register(model);
        }

        [HttpGet("id")]
        [Authorize]
        public async Task<UserResponseModel> GetUserByIdAsync([FromHeader] int id)
        {
            return await _userService.GetUserByIdAsync(id);
        }
    }
}